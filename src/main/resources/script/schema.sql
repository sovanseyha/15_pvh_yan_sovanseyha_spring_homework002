CREATE TABLE customers_tb(
                            customer_id SERIAL PRIMARY KEY,
                            customer_name VARCHAR(255),
                            customer_address VARCHAR(255),
                            customer_phone VARCHAR(20)
);

CREATE TABLE product_tb(
                           product_id serial PRIMARY KEY,
                           product_name varchar,
                           product_price double precision
);
CREATE TABLE invoice_tb(
                           invoice_id serial PRIMARY KEY,
                           invoice_date date,
                           customer_id integer, CONSTRAINT fk_customer foreign key (customer_id) references customers_tb (customer_id)
        on delete cascade on update cascade
);

CREATE TABLE invoice_detail_tb(
                                  id serial PRIMARY KEY,
                                  invoice_id integer, CONSTRAINT fk_invoice foreign key (invoice_id) references invoice_tb (invoice_id)
        on delete cascade on update cascade,
                                  product_id integer, CONSTRAINT fk_product foreign key (product_id) references product_tb (product_id)
        on delete cascade on update cascade
)