package com.homework.spring_homework2.service.implement;

import com.homework.spring_homework2.model.entity.Customer;
import com.homework.spring_homework2.model.request.CustomerRequest;
import com.homework.spring_homework2.model.response.CustomerResponse;
import com.homework.spring_homework2.repository.CustomerRepository;
import com.homework.spring_homework2.service.CustomerService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@Service
public class CustomerServiceImp implements CustomerService {

    private final CustomerRepository customerRepository;

    public CustomerServiceImp(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public List<Customer> getAllCustomers() {
        return customerRepository.findAllCustomer();
    }

    @Override
    public Customer getCustomerById(Integer customerId) {
        return customerRepository.getCustomerById(customerId);
    }

    @Override
    public boolean deleteCustomerById(Integer customerId) {
        return customerRepository.deleteCustomerById(customerId);
    }

    @Override
    public Integer addNewCustomer(CustomerRequest customerRequest) {
        Integer customerId = customerRepository.saveCustomer(customerRequest);
        return customerId;
    }

    @Override
    public Integer updateCustomer(CustomerRequest customerRequest, Integer customerId) {
        Integer customerIdUpdate = customerRepository.updateCustomer(customerRequest, customerId);
        return customerIdUpdate;
    }


}
