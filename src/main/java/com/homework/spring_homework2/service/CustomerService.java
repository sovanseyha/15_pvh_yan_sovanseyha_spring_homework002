package com.homework.spring_homework2.service;

import com.homework.spring_homework2.model.entity.Customer;
import com.homework.spring_homework2.model.request.CustomerRequest;

import java.util.List;

public interface CustomerService {

//    show all customer
    List<Customer> getAllCustomers();

//    get customer by id
    Customer getCustomerById(Integer customerId);

    //delete
    boolean deleteCustomerById(Integer customerId);

    //insert
    Integer addNewCustomer(CustomerRequest customerRequest);

    //update
    Integer updateCustomer(CustomerRequest customerRequest, Integer customerId);
}