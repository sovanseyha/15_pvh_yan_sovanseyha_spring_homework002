package com.homework.spring_homework2.service;

import com.homework.spring_homework2.model.entity.Product;
import com.homework.spring_homework2.model.request.ProductRequest;

import java.util.List;

public interface ProductService {
//    show all product
    List<Product> getAllProducts();
//    get customer by id
    Product getProductById(Integer productId);
//    delete by id
    boolean deleteProductById(Integer productId);
//    insert
    Integer addNewProduct(ProductRequest productRequest);
//    update product
    Integer updateProduct(ProductRequest productRequest, Integer productId);
}

