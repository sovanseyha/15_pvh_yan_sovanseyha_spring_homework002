package com.homework.spring_homework2.controller;

import com.homework.spring_homework2.model.entity.Customer;
import com.homework.spring_homework2.model.request.CustomerRequest;
import com.homework.spring_homework2.model.response.CustomerResponse;
import com.homework.spring_homework2.service.CustomerService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class CustomerController {

    private final CustomerService customerService;

    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping("/get-all-customer")
    @Operation(summary = "Get all Customers")
    public ResponseEntity<CustomerResponse<List<Customer>>> getAllCustomer(){

        CustomerResponse<List<Customer>> response = CustomerResponse.<List<Customer>>builder()
                .payload(customerService.getAllCustomers())
//                .httpStatus(HttpStatus.OK)
                .message("Fetch data successfully")
                .success(true)
//                .timestamp(new Timestamp(System.currentTimeMillis()))
                .build();

        return ResponseEntity.ok(response);
    }

//    get customer by id
    @GetMapping("/get-customer-by-id/{id}")
    @Operation(summary="Get Customer by ID")
    public ResponseEntity<CustomerResponse<Customer>> getCustomerById(@PathVariable("id") Integer customerId){

        CustomerResponse<Customer> response = null;
        if (customerService.getCustomerById(customerId) != null){
            response = CustomerResponse.<Customer>builder()
                    .message("Success fetch data by ID")
                    .payload(customerService.getCustomerById(customerId))
                    .success(true)
                    .build();
            return ResponseEntity.ok(response);
        }
        else {
            response = CustomerResponse.<Customer>builder()
                    .message("Data not found")
                    .success(false)
//                    .httpStatus(HttpStatus.NOT_FOUND)
                    .build();
            return ResponseEntity.badRequest().body(response);
        }
    }

//    delete
    @DeleteMapping("/delete-customer-by-id/{id}")
    @Operation(summary = "Delete Customer by ID")
    public ResponseEntity<CustomerResponse<String>> deleteCustomerById(@PathVariable("id") Integer customerId){
        CustomerResponse<String> response = null;
        if (customerService.deleteCustomerById(customerId) == true) {
            response = CustomerResponse.<String>builder()
                    .message("Delete successfully")
                    .success(true)
                    .build();
            return ResponseEntity.ok(response);
        }
        else {
            response = CustomerResponse.<String>builder()
                    .message("Data not found delete unsuccessful")
                    .success(false)
                    .build();
            return ResponseEntity.badRequest().body(response);
        }
    }

    @PostMapping("add-new-customer")
    @Operation(summary = "Save new customer")
    public ResponseEntity<CustomerResponse<Customer>> addNewCustomer(@RequestBody CustomerRequest customerRequest){
        Integer storeCustomerId = customerService.addNewCustomer(customerRequest);

        if (storeCustomerId != null){
            CustomerResponse<Customer> response = CustomerResponse.<Customer>builder()
                    .message("Add succesfully")
                    .success(true)
                    .payload(customerService.getCustomerById(storeCustomerId))
                    .build();
            return ResponseEntity.ok(response);
        }
        return null;
    }

    //update
    @PutMapping("/update-customer-by-id/{id}")
    @Operation(summary = "Update Customer by ID")
    public ResponseEntity<CustomerResponse<Customer>> updateCustomerById(
            @RequestBody CustomerRequest customerRequest,
            @PathVariable("id") Integer customerId
    ) {
        CustomerResponse<Customer> response = null;
        Integer idCustomerUpdate = customerService.updateCustomer(customerRequest, customerId);
        if (idCustomerUpdate != null) {
            response = CustomerResponse.<Customer>builder()
                    .payload(customerService.getCustomerById(idCustomerUpdate))
                    .message("Update Successfully")
                    .success(true)
                    .build();
            return ResponseEntity.ok(response);
        }
        else {
            response = CustomerResponse.<Customer>builder()
                    .message("Data not found update unsuccessful")
                    .success(false)
                    .build();
            return ResponseEntity.badRequest().body(response);
        }
    }
}