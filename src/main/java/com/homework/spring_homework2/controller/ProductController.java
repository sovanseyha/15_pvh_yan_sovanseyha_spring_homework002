package com.homework.spring_homework2.controller;

import com.homework.spring_homework2.model.entity.Product;
import com.homework.spring_homework2.model.request.ProductRequest;
import com.homework.spring_homework2.model.response.ProductResponse;
import com.homework.spring_homework2.service.ProductService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class ProductController {
    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping("/get-all-product")
    @Operation(summary = "Get all Products")
    public ResponseEntity<ProductResponse<List<Product>>> getAllProduct() {
        ProductResponse<List<Product>> respone = ProductResponse.<List<Product>>builder()
                .payload(productService.getAllProducts())
                .message("Fetch data successfully")
                .success(true)
                .build();
        return ResponseEntity.ok(respone);
    }

    //    get product by id
    @GetMapping("/get-product-by-id/{id}")
    @Operation(summary = "Get Product by ID")
    public ResponseEntity<ProductResponse<Product>> getProductById(@PathVariable("id") Integer productId) {
        ProductResponse<Product> response = null;
        if (productService.getProductById(productId) != null) {
            response = ProductResponse.<Product>builder()
                    .message("Success fetch data by ID")
                    .payload(productService.getProductById(productId))
                    .success(true)
                    .build();
            return ResponseEntity.ok(response);
        } else {
            response = ProductResponse.<Product>builder()
                    .message("Data not found")
                    .success(false)
                    .build();
            return ResponseEntity.badRequest().body(response);
        }
    }


    //    delete
    @DeleteMapping("/delete-product-by-id/{id}")
    @Operation(summary = "Delete Product by ID")
    public ResponseEntity<Object> deleteProductById(@PathVariable("id") Integer productId){
        ProductResponse<String> response = null;
        if (productService.deleteProductById(productId) == true) {
            response = ProductResponse.<String>builder()
                    .message("Delete successfully")
                    .success(true)
                    .build();
            return ResponseEntity.ok(response);
        }
        else {
            response = ProductResponse.<String>builder()
                    .message("Data not found delete unsuccessful")
                    .success(false)
                    .build();
            return ResponseEntity.badRequest().body(response);
        }
    }

//    insert new product
@PostMapping("add-new-product")
@Operation(summary = "Save new product")
public ResponseEntity<Object> addNewProduct(@RequestBody ProductRequest productRequest){
    Integer storeProductId = productService.addNewProduct(productRequest);

    if (storeProductId != null){
        ProductResponse<Product> response = ProductResponse.<Product>builder()
                .message("Add succesfully")
                .success(true)
                .payload(productService.getProductById(storeProductId))
                .build();
        return ResponseEntity.ok(response);
    }
    return null;
}


    //update
    @PutMapping("/update-product-by-id/{id}")
    @Operation(summary = "Update Product by ID")
    public ResponseEntity<ProductResponse<Product>> updateProductById(
            @RequestBody ProductRequest productRequest,
            @PathVariable("id") Integer productId
    ) {
        ProductResponse<Product> response = null;
        Integer idProductUpdate = productService.updateProduct(productRequest, productId);

        if (idProductUpdate != null) {
            response = ProductResponse.<Product>builder()
                    .payload(productService.getProductById(idProductUpdate))
                    .message("Update Successfully")
                    .success(true)
                    .build();
            return ResponseEntity.ok(response);
        }
        else {
            response = ProductResponse.<Product>builder()
                    .message("Data not found update unsuccessful")
                    .success(false)
                    .build();
            return ResponseEntity.badRequest().body(response);
        }
    }

}
