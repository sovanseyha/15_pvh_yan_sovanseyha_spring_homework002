package com.homework.spring_homework2.model.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.sun.net.httpserver.Authenticator;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

import java.sql.Timestamp;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CustomerResponse<T> {
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private T payload;
//    private HttpStatus httpStatus;
//    private Timestamp timestamp;
    private String message;
    private boolean success;
}