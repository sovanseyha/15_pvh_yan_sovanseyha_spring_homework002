package com.homework.spring_homework2.model.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ProductResponse<T> {
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private T payload;
    private String message;
    private boolean success;
}
