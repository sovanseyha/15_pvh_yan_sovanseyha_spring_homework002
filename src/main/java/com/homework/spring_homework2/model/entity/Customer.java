package com.homework.spring_homework2.model.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data //getterSetter
@AllArgsConstructor //parameterize constructor
@NoArgsConstructor //default constructor
@Builder //to bind data
public class Customer {
    //wrapper class
    private Integer customerId;
    private String customerName;
    private String customerAddress;
    private String customerPhone;
}
