package com.homework.spring_homework2.repository;

import com.homework.spring_homework2.model.entity.Customer;
import com.homework.spring_homework2.model.request.CustomerRequest;
import lombok.Data;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface CustomerRepository {
    @Select("SELECT * FROM customers_tb")
    @Results(id = "customerMap", value = {
            @Result(property = "customerId", column = "customer_id"),
            @Result(property = "customerName", column = "customer_name"),
            @Result(property = "customerAddress", column = "customer_address"),
            @Result(property = "customerPhone", column = "customer_phone")
    })
    List<Customer> findAllCustomer();

    @Select("SELECT * FROM customers_tb WHERE customer_id = #{customerId}")
    @ResultMap("customerMap")
    Customer getCustomerById(Integer customerId);

    //delete by id
//    @Select ("RETURNING id")
//      @Insert
//      @Delete
//      @Update
    @Delete("DELETE FROM customers_tb WHERE customer_id = #{customerId}")
    @ResultMap("customerMap")
    boolean deleteCustomerById(Integer customerId);
//    @Delete("DELETE * FROM customers_tb WHERE customer_id = #{id}")
//    boolean deleteCustomerById(@Param("id") Integer customerId);

    //insert
    @Insert("INSERT INTO customers_tb (customer_name, customer_address, customer_phone) VALUES (#{request.customerName}, #{request.customerAddress}, #{request.customerPhone} )"
    + " RETURNING customer_id")
    @ResultMap("customerMap")
    Integer saveCustomer(@Param("request") CustomerRequest customerRequest);

    //update
//    @Select("UPDATE customers_tb " +
//            "SET customer_name = #{request.customerName}, " +
//            "customer_address = #{request.customerAddress}, " +
//            "customer_phone = #{request.customerPhone} " +
//            "WHERE customer_id = #{customerId} " +
//            "RETURNING customer_id")
    @Select("""
            UPDATE customers_tb
                        SET customer_name = #{request.customerName},
                        customer_address = #{request.customerAddress},
                        customer_phone = #{request.customerPhone}
                        WHERE customer_id = #{customerId}
                        RETURNING customer_id
            """)
    @Result(property = "customerId", column = "customer_id")
    Integer updateCustomer(@Param("request") CustomerRequest customerRequest, Integer customerId);
}
