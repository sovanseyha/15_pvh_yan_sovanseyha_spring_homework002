package com.homework.spring_homework2.repository;

import com.homework.spring_homework2.model.entity.Product;
import com.homework.spring_homework2.model.request.ProductRequest;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface ProductRepository {
    @Select("SELECT * FROM product_tb")
    @Results(id = "productMap", value = {
            @Result(property = "productId", column = "product_id"),
            @Result(property = "productName", column = "product_name"),
            @Result(property = "productPrice", column = "product_price")
    })
    List<Product> findAllProduct();

    @Select("SELECT * FROM product_tb WHERE product_id = #{productId}")
    @ResultMap("productMap")
    Product getProductById(Integer productId);

    @Delete("DELETE FROM product_tb WHERE product_id = #{productId}")
    @ResultMap("productMap")
    boolean deleteProductById(Integer productId);

    @Insert("INSERT INTO product_tb (product_name, product_price) VALUES (#{request.productName}, #{request.productPrice} )"
            + " RETURNING product_id")
    @ResultMap("productMap")
    Integer saveProduct(@Param("request") ProductRequest customerRequest);

    @Select("""
            UPDATE product_tb
                        SET product_name = #{request.productName},
                        product_price = #{request.productPrice}
                        WHERE product_id = #{productId}
                        RETURNING product_id
            """)
    @Result(property = "productId", column = "product_id")
    Integer updateProduct(@Param("request") ProductRequest productRequest, Integer productId);
}
